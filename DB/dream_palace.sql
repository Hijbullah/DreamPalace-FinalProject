-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2017 at 10:55 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dream_palace`
--
CREATE DATABASE IF NOT EXISTS `dream_palace` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `dream_palace`;

-- --------------------------------------------------------

--
-- Table structure for table `property_sale`
--

DROP TABLE IF EXISTS `property_sale`;
CREATE TABLE `property_sale` (
  `id` int(13) NOT NULL,
  `owners_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `local_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `property_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `property_picture` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `min_bed` int(10) NOT NULL,
  `min_bath` int(10) NOT NULL,
  `facilities` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `property_size` int(10) NOT NULL,
  `property_price` int(10) NOT NULL,
  `property_status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `property_sale`
--

INSERT INTO `property_sale` (`id`, `owners_name`, `email`, `phone`, `division`, `district`, `local_city`, `property_name`, `property_picture`, `description`, `min_bed`, `min_bath`, `facilities`, `property_size`, `property_price`, `property_status`, `date_time`, `is_trashed`) VALUES
(11, 'hijbullah', 'hijbullaah@gmail.com', '01858078583', 'Dhaka', 'Kishorgonj', 'Chawk Bazar', 'My Villa', '1508189961Lighthouse.jpg, 1508189961Desert.jpg, 1508189961Hydrangeas.jpg, 1508189961Jellyfish.jpg, 1508189961Penguins.jpg, 1508189961Tulips.jpg', 'this is some description this is some description this is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some description', 0, 0, 'Semi Furnished', 2400, 24000, 'Sale', '2017-10-18 05:43:29', 'NO'),
(12, 'hijbullah', 'hijbullaah@gmail.com', '01858078583', 'Dhaka', 'Kishorgonj', 'Chawk Bazar', 'My Villa', '1508190232Lighthouse.jpg, 1508190232Desert.jpg, 1508190232Hydrangeas.jpg, 1508190232Jellyfish.jpg, 1508190232Penguins.jpg, 1508190232Tulips.jpg', 'this is some description this is some description this is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some descriptionthis is some description', 0, 0, 'Semi Furnished', 2400, 24000, 'Sale', '2017-10-18 05:43:29', 'NO'),
(13, 'Karim', 'karim.hasnat@gmail.com', '015455686654', 'Dhaka', 'Madaripur', 'MuradPur', 'name 2', '1508219138Chrysanthemum.jpg, 1508219138Lighthouse.jpg, 1508219138Penguins.jpg, 1508219138Tulips.jpg', 'khlkjfahkjlaag', 50, 0, 'not Furnished', 40, 25000, 'Rent', '2017-10-18 05:43:29', 'NO'),
(14, 'Piaro', 'piaro22440@gmail.com', '018589545454', 'Barishal', 'Barguna', 'Barguna', 'Momo Kunjo', '1508261706Tulips.jpg, 1508261706Chrysanthemum.jpg, 1508261706Desert.jpg, 1508261706Koala.jpg, 1508261706Lighthouse.jpg', 'description description description', 5, 5, 'Semi Furnished', 18000, 18000, 'Sale', '2017-10-25 05:26:38', 'NO'),
(15, 'Sumaiya', 'sample@gmail.com', '02545524', 'Chittagong', 'Chittagong', 'Bohaddar  Hat', 'Santi nir', '1508305560Hydrangeas.jpg, 1508305560Jellyfish.jpg, 1508305560Tulips.jpg', 'this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description this is description ', 10, 5, 'not Furnished', 1700, 18000, 'Rent', '2017-10-18 05:46:00', 'NO'),
(16, 'alan parker', '1234@gmail.com', '01857625436', 'Chittagong', 'Chittagong', 'chawkbazer', 'disney world', '1508912460img.jpg, 1508912460Photograp5h.jpg, 1508912460Photograph3.jpg, 1508912460Photograph4.jpg', 'hytfuyt;idsyf9o\'7ueptruhyelikgtdsjg', 475, 185, 'Full Furnished', 382123254, 451000, 'Rent', '2017-10-25 06:21:00', 'NO'),
(17, 'alan parker', '1234@gmail.com', '01857625436', 'Chittagong', 'Chittagong', 'chawkbazer', 'disney world', '1508912472img.jpg, 1508912472Photograp5h.jpg, 1508912472Photograph3.jpg, 1508912472Photograph4.jpg', 'hytfuyt;idsyf9o\'7ueptruhyelikgtdsjg', 475, 185, 'Full Furnished', 382123254, 451000, 'Rent', '2017-10-25 06:21:12', 'NO'),
(18, 'Hijbullah', 'hijbullaah@gmail.com', '44646454', 'Chittagong', 'Faridpur', 'Chawk Bazar', 'Flat for sale', '1508913587Photograph4.jpg, 1508913587img.jpg, 1508913587Photograp5h.jpg, 1508913587Photograph.1jpg.jpg, 1508913587Photograph2.jpg', 'this is some description', 4, 5, 'Semi Furnished', 2400, 24000, 'Sale', '2017-10-25 06:39:47', 'NO'),
(19, 'Hijbullah', 'hijbullaah@gmail.com', '44646454', 'Chittagong', 'Faridpur', 'Chawk Bazar', 'Flat for sale', '1508913642Photograph4.jpg, 1508913642img.jpg, 1508913642Photograp5h.jpg, 1508913642Photograph.1jpg.jpg, 1508913642Photograph2.jpg', 'this is some description', 4, 5, 'Semi Furnished', 2400, 24000, 'Sale', '2017-10-25 06:40:42', 'NO'),
(20, 'Hijbullah', 'hijbullaah@gmail.com', '4646545635', 'Chittagong', 'Gazipur', 'Chawk bazar', 'flat for sle', '1508913766img.jpg, 1508913766Photograp5h.jpg, 1508913766Photograph.1jpg.jpg', 'this is des', 5, 4, 'Full Furnished', 12400, 24000, 'Sale', '2017-10-25 06:42:46', 'NO'),
(21, 'Hijbullah', 'hijbullaah@gmail.com', '4646545635', 'Chittagong', 'Gazipur', 'Chawk bazar', 'flat for sle', '1508913812img.jpg, 1508913812Photograp5h.jpg, 1508913812Photograph.1jpg.jpg', 'this is des', 5, 4, 'Full Furnished', 12400, 24000, 'Sale', '2017-10-25 06:43:32', 'NO'),
(22, 'Hijbullah', 'hijbullaah@gmail.com', '4646545635', 'Chittagong', 'Gazipur', 'Chawk bazar', 'flat for sle', '1508913882img.jpg, 1508913882Photograp5h.jpg, 1508913882Photograph.1jpg.jpg', 'this is des', 5, 4, 'Full Furnished', 12400, 24000, 'Sale', '2017-10-25 06:44:42', 'NO'),
(23, 'Hijbullah', 'hijbullaah@gmail.com', '4646545635', 'Chittagong', 'Gazipur', 'Chawk bazar', 'flat for sle', '1508913903img.jpg, 1508913903Photograp5h.jpg, 1508913903Photograph.1jpg.jpg', 'this is des', 5, 4, 'Full Furnished', 12400, 24000, 'Sale', '2017-10-25 06:45:03', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(13) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `email_verified`) VALUES
(8, 'Hijbullah', 'hijbullaah@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'YES');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `property_sale`
--
ALTER TABLE `property_sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `property_sale`
--
ALTER TABLE `property_sale`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
