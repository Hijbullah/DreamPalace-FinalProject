<?php
if(!isset($_SESSION) )session_start();
require_once ("../vendor/autoload.php");
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$objUser = new User();
$objUser->setData($_SESSION);
$oneData = $objUser->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();


?>





<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dream Palace | Contact</title>
    <meta name="description" content="company is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="../resources/assets/css/normalize.css">
    <link rel="stylesheet" href="../resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../resources/assets/css/fontello.css">
    <link href="../resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="../resources/assets/css/price-range.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="../resources/assets/css/style.css">
    <link rel="stylesheet" href="../resources/assets/css/responsive.css">
</head>
<body>

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- Body content -->


<!--Message div-->

<?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>
    <div  id="message" class="form button">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo "&nbsp;".Message::message();
        }
        Message::message(NULL);
        ?>
    </div>
<?php } ?>

<!--Message div-->

<div class="header-connect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  col-xs-12">
                <div class="header-half header-call">
                    <p>
                        <span><i class="pe-7s-call"></i> +8801858078583</span>
                        <span><i class="pe-7s-mail"></i>bengalcoders@gmail.com</span>
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12 text-right">

                <?php

                if ($status){
                    echo "Welcome: "."<a href=''>".$oneData->name."</a>";
                }else{
                    echo "<a href='register.php'>LogIn</a>";
                }

                ?>

            </div>
        </div>
    </div>
</div>
<!--End top header -->

<nav class="navbar navbar-default ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="../resources/assets/img/logo.png" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <div class="button navbar-right">
                <?php
                if ($status){
                    echo '<button class="navbar-btn nav-button wow bounceInRight login" onclick=" location.replace(\'User/Authentication/logout.php\')" data-wow-delay="0.4s">LogOut</button>';
                }else{
                    echo '<button class="navbar-btn nav-button wow bounceInRight login" onclick=" location.replace(\'register.php\')" data-wow-delay="0.4s">Login</button>';
                }
                ?>
                <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('submit-property.php')" data-wow-delay="0.5s">Submit</button>
            </div>
            <ul class="main-nav nav navbar-nav navbar-right">
                <li class="dropdown ymm-sw " data-wow-delay="0.1s"><a href="index.php">Home</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a href="properties-sale.php">Properties For Sale</a></li>
                <li class="wow fadeInUp" data-wow-delay="0.1s"><a  href="properties-rent.php">Properties For Rent</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="contact.php"  class="active">Contact</a></li>
                <?php
                if ($status){
                    echo ' <li class="dropdown ymm-sw" data-wow-delay="0.4s">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">My Account <b class="caret"></b></a>
                                        <ul class="dropdown-menu navbar-nav">
                                           
                                            <li>
                                                <a href="user-properties.php">My Property</a>
                                            </li>
                                          
                                        </ul>
                                    </li>';
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- End of nav bar -->


<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Contact With US!</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<!-- property area -->
<div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
    <div class="container">

        <div class="col-md-9">

            <div class="" id="contact1">
                <div class="row">
                    <div class="col-sm-4">
                        <h3><i class="fa fa-map-marker"></i> Address</h3>
                        <p>Bengal Coders
                            <br>PHP-B69
                            <br>BITM
                            <br>Chittagong
                            <br>
                            <strong>Bangladesh</strong>
                        </p>
                    </div>
                    <!-- /.col-sm-4 -->
                    <div class="col-sm-4">
                        <h3><i class="fa fa-phone"></i> Call center</h3>
                        <p class="text-muted">This number is toll free if calling from Great Britain otherwise we advise you to use the electronic form of communication.</p>
                        <p><strong>+8801858078583</strong></p>
                    </div>
                    <!-- /.col-sm-4 -->
                    <div class="col-sm-4">
                        <h3><i class="fa fa-envelope"></i> Electronic support</h3>
                        <p class="text-muted">Please feel free to write an email to us or to use our electronic ticketing system.</p>
                        <ul>
                            <li><strong><a href="mailto:bengalcoders@gmail.com">bengalcoders@gmail.com</a></strong>   </li>
                        </ul>
                    </div>
                    <!-- /.col-sm-4 -->
                </div>
                <!-- /.row -->

                <hr>
                <h2>Contact form</h2>
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstname">Firstname</label>
                                <input type="text" class="form-control" id="firstname">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastname">Lastname</label>
                                <input type="text" class="form-control" id="lastname">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="message-contact">Message</label>
                                <textarea id="message-contact" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>
                        </div>
                    </div>
                    <!-- /.row -->
                </form>
            </div>
        </div>
        <!-- /.col-md-9 -->

        <div class="col-md-3 ">
            <div class="blog-asside-right">
                <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recommended</h3>
                    </div>
                    <div class="panel-body recent-property-widget">
                        <ul>
                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-2.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-1.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-3.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-2.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-2.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 blg-thumb p0">
                                    <a href="single.html"><img src="../resources/assets/img/demo/small-property-2.jpg"></a>
                                    <span class="property-seeker">
                                                <b class="b-1">A</b>
                                                <b class="b-2">S</b>
                                            </span>
                                </div>
                                <div class="col-md-8 blg-entry">
                                    <h6> <a href="single.html">Super nice villa </a></h6>
                                    <span class="property-price">3000000$</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer area-->
<div class="footer-area">

    <div class=" footer">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>About us </h4>
                        <div class="footer-title-line"></div>

                        <img src="../resources/assets/img/footer-logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                        <p>Lorem ipsum dolor cum necessitatibus su quisquam molestias. Vel unde, blanditiis.</p>
                        <ul class="footer-adress">
                            <li><i class="pe-7s-map-marker strong"> </i> PHP-B69, BITM, CTG</li>
                            <li><i class="pe-7s-mail strong"> </i> bengalcoders@gmail.com</li>
                            <li><i class="pe-7s-call strong"> </i> +8801858078583</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links </h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-menu">
                            <li><a href="properties-sale.php">Properties For Sale</a>  </li>
                            <li><a href="properties-rent.php">Properties For Rent</a>  </li>
                            <li><a href="submit-property.php">Submit property </a></li>
                            <li><a href="contact.php">Contact us</a></li>
                            <li><a href="register.php">Log In </a>  </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Last News</h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-blog">
                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer news-letter">
                        <h4>Stay in touch</h4>
                        <div class="footer-title-line"></div>
                        <p>Lorem ipsum dolor sit amet, nulla  suscipit similique quisquam molestias. Vel unde, blanditiis.</p>

                        <form>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="E-mail ... ">
                                <span class="input-group-btn">
                                            <button class="btn btn-primary subscribe" type="button"><i class="pe-7s-paper-plane pe-2x"></i></button>
                                        </span>
                            </div>
                            <!-- /input-group -->
                        </form>

                        <div class="social pull-right">
                            <ul>
                                <li><a class="wow fadeInUp animated" href="https://twitter.com/kimarotec"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-copy text-center">
        <div class="container">
            <div class="row">
                <div class="pull-left">
                    <span> (C) <a href="http://www.Facebook.com/Hijbuu">Dream Palace</a> , All rights reserved 2016  </span>
                </div>
                <div class="bottom-menu pull-right">
                    <ul>
                        <li><a class="wow fadeInUp animated" href="index.php" data-wow-delay="0.2s">Home</a></li>
                        <li><a class="wow fadeInUp animated" href="properties-sale.php" data-wow-delay="0.3s">Properties for Sale</a></li>
                        <li><a class="wow fadeInUp animated" href="properties-rent.php" data-wow-delay="0.3s">Properties for Rent</a></li>
                        <li><a class="wow fadeInUp animated" href="contact.php" data-wow-delay="0.6s">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="../resources/assets/js/modernizr-2.6.2.min.js"></script>

<script src="../resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="../resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../resources/assets/js/bootstrap-select.min.js"></script>
<script src="../resources/assets/js/bootstrap-hover-dropdown.js"></script>

<script src="../resources/assets/js/easypiechart.min.js"></script>
<script src="../resources/assets/js/jquery.easypiechart.min.js"></script>

<script src="../resources/assets/js/owl.carousel.min.js"></script>
<script src="../resources/assets/js/wow.js"></script>

<script src="../resources/assets/js/icheck.min.js"></script>
<script src="../resources/assets/js/price-range.js"></script>
<script src="../resources/jquery-ui-1.12.1/jquery-ui.js"></script>

<script src="../resources/assets/js/main.js"></script>

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>


<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->

</body>
</html>