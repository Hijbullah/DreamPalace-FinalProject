<?php
    if(!isset($_SESSION) )session_start();
    require_once ("../vendor/autoload.php");

    use App\PropertySale\PropertySale;
    use App\Utility\Utility;
    use App\Message\Message;

    $obj = new PropertySale();


    $allPictureName = $_FILES['PropertyPicture']['name'];
    $allPictureTemp = $_FILES['PropertyPicture']['tmp_name'];
    $allPicture = array_combine($allPictureName, $allPictureTemp);




    foreach($allPicture as $key=>$value){

        $fileName[] =   time().$key;

        // Start of physically moving file to its destination

        $picName = time().$key;
        $source = $value;
        $destination = "../resources/UploadedPicture/".$picName;
        move_uploaded_file($source, $destination);

        // End of physically moving file to its destination
    }

// Start of the process to store file name to the table

$strPic =  implode(", ", $fileName);

$_POST["PropertyPicture"] = $strPic;
$obj->setData($_POST);
$obj->store();

// End of the process to store file name to the table

Utility::redirect("user-properties.php");






