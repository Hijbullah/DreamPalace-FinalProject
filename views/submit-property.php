<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$oneData = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('register.php');
    return;
}



?>




<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GARO ESTATE | Submit property Page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="../resources/assets/css/normalize.css">
    <link rel="stylesheet" href="../resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../resources/assets/css/fontello.css">
    <link href="../resources/../resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="../resources/assets/css/price-range.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="../resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="../resources/assets/css/wizard.css">
    <link rel="stylesheet" href="../resources/assets/css/style.css">
    <link rel="stylesheet" href="../resources/assets/css/responsive.css">
</head>
<body>

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- Body content -->

<!--Message div-->

    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>
        <div  id="message" class="form button">
            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                echo "&nbsp;".Message::message();
            }
            Message::message(NULL);
            ?>
        </div>
    <?php } ?>

<!--Message div-->

<div class="header-connect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  col-xs-12">
                <div class="header-half header-call">
                    <p>
                        <span><i class="pe-7s-call"></i> +8801858078583</span>
                        <span><i class="pe-7s-mail"></i>bengalcoders@gmail.com</span>
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12 text-right">

                <?php

                if ($status){
                    echo "Welcome: "."<a href=''>".$oneData->name."</a>";
                }else{
                    echo "<a href='register.php'>LogIn</a>";
                }

                ?>

            </div>
        </div>
    </div>
</div>
<!--End top header -->

<nav class="navbar navbar-default ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="../resources/assets/img/logo.png" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <div class="button navbar-right">
                <?php
                if ($status){
                    echo '<button class="navbar-btn nav-button wow bounceInRight login" onclick=" location.replace(\'User/Authentication/logout.php\')" data-wow-delay="0.4s">LogOut</button>';
                }else{
                    echo '<button class="navbar-btn nav-button wow bounceInRight login" onclick=" location.replace(\'register.php\')" data-wow-delay="0.4s">Login</button>';
                }
                ?>
                <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('submit-property.php')" data-wow-delay="0.5s">Submit</button>
            </div>
            <ul class="main-nav nav navbar-nav navbar-right">
                <li class="dropdown ymm-sw " data-wow-delay="0.1s"><a href="index.php" class="active">Home</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="properties-sale.php">Properties For Sale</a></li>
                <li class="wow fadeInUp" data-wow-delay="0.1s"><a class="" href="properties-rent.php">Properties For Rent</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="contact.php">Contact</a></li>
                <?php
                if ($status){
                    echo ' <li class="dropdown ymm-sw" data-wow-delay="0.4s">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">My Account <b class="caret"></b></a>
                                        <ul class="dropdown-menu navbar-nav">
                                            <li>
                                                <a href="user-profile.php">My Profile</a>
                                            </li>
                                            <li>
                                                <a href="user-properties.php">My Property</a>
                                            </li>
                                            <li>
                                                <a href="change-password.php">Change Password</a>
                                            </li>
                                        </ul>
                                    </li>';
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- End of nav bar -->

<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Submit new property</h1>
                <h2 class="page-title"><?php echo "Hello ". $oneData->name; ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<!-- property area -->
<div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
    <div class="container">
        <div class="clearfix" >
            <div class="wizard-container">

                <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                    <form data-toggle="validator" id="SubmitForm" method="POST" enctype="multipart/form-data">
                        <div class="wizard-header">
                            <h3>
                                <b>Submit</b> YOUR PROPERTY <br>
                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing.</small>
                            </h3>
                        </div>

                        <ul>
                            <li><a href="#step1" data-toggle="tab">Step 1 </a></li>
                            <li><a href="#step2" data-toggle="tab">Step 2 </a></li>
                            <li><a href="#step3" data-toggle="tab">Step 3 </a></li>
                            <li><a href="#step4" data-toggle="tab">Finished </a></li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane" id="step1">
                                <div class="row p-b-15  ">
                                    <h4 class="info-text"> Let's start with the basic information (with validation)</h4>
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <div class="picture-container">
                                            <div class="picture">
                                                <img src="../resources/assets/img/default-property.jpg" class="picture-src" id="wizardPicturePreview" title=""/>
                                                <input type="file" name="PropertyPicture[]" id="wizard-picture" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Owner's Name <small>(empty if you wanna use default name)</small></label>
                                            <input name="OwnersName" type="text" value="<?php echo $oneData->name ?>" id="name" class="form-control" placeholder=" Enter Your Name ..." required >
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email <small>(empty it for default email)</small></label>
                                            <input name="Email" type="email" id="email" value="<?php echo $oneData->email ?>" class="form-control" placeholder="example@gmail.com" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Telephone <small>(empty if you wanna use default phone number)</small></label>
                                            <input name="Phone" type="text" id="phone" class="form-control" placeholder="+8801858078583" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="PropertyName">Post Title <small>(required)</small></label>
                                            <input name="PropertyName" type="text" id="PropertyName" class="form-control" placeholder="Super villa ..." required >
                                        </div>

                                        <div class="form-group">
                                            <label for="PropertySize">Property Size <small>(required)</small></label>
                                            <input name="PropertySize" type="text" id="PropertySize" class="form-control" placeholder="3330000">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 1 -->

                            <div class="tab-pane" id="step2">
                                <h4 class="info-text"> How much your Property is Beautiful ? </h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="Description">Property Description :</label>
                                                <textarea name="Description" id="Description" class="form-control" ></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Division">Division:</label>
                                                <select id="Division" name="Division" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your Division">
                                                    <option value="Dhaka">Dhaka</option>
                                                    <option value="Chittagong">Chittagong</option>
                                                    <option value="Khulna">Khulna</option>
                                                    <option value="Rajshahi">Rajshahi</option>
                                                    <option value="Barishal">Barishal</option>
                                                    <option value="Sylhet">Sylhet</option>
                                                    <option value="Rangput">Rangput</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="District">District:</label>
                                                <select id="District" name="District" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your District">
                                                    <optgroup label="Dhaka Division">
                                                        <option value="Dhaka">Dhaka </option>
                                                        <option value="Faridpur">Faridpur </option>
                                                        <option value="Gazipur">Gazipur </option>
                                                        <option value="Gopalgonj">Gopalganj </option>
                                                        <option value="Jamalpur"> Jamalpur</option>
                                                        <option value="Kishorgonj">Kishorgonj </option>
                                                        <option value="Madaripur"> Madaripur</option>
                                                        <option value="Manikgonj"> Manikgonj</option>
                                                        <option value="Munshigonj"> Munshigonj</option>
                                                        <option value="Mymenshingh">Mymenshingh</option>
                                                        <option value="Narayanganj">Narayanganj </option>
                                                        <option value="Narsingdi">Narsingdi </option>
                                                        <option value="Netrokona">Netrokona </option>
                                                        <option value="Rajbari"> Rajbari</option>
                                                        <option value="Shariatpur">Shariatpur </option>
                                                        <option value="Sherpur">Sherpur </option>
                                                        <option value="Tangail">Tangail </option>
                                                    </optgroup>

                                                    <optgroup label="Chittagong Division">
                                                        <option value="Bandarban"> Bandarban</option>
                                                        <option value="Bhahmanbaria">Bhahmanbaria </option>
                                                        <option value="Chandpur"> Chandpur</option>
                                                        <option value="Chittagong">Chittagong </option>
                                                        <option value="Comilla">Comilla </option>
                                                        <option value="Cox's Bazar">Cox's Bazar </option>
                                                        <option value="Feni">Feni </option>
                                                        <option value="Khagrachari">Khagrachari </option>
                                                        <option value="Lakshmipur">Lakshmipur </option>
                                                        <option value="Noakhali">Noakhali </option>
                                                        <option value="Rangamati"> Rangamati</option>
                                                    </optgroup>

                                                    <optgroup label="Khulna Division">
                                                        <option value="Bagerhat">Bagerhat </option>
                                                        <option value="Chuadanga"> Chuadanga</option>
                                                        <option value="Jessore">Jessore </option>
                                                        <option value="Jhenadha">Jhenadha </option>
                                                        <option value="Khulna"> Khulna</option>
                                                        <option value="Kustia">Kustia </option>
                                                        <option value="Magura">Magura </option>
                                                        <option value="Meherpur">Meherpur </option>
                                                        <option value="Narail">Narail </option>
                                                        <option value="Satkhira">Satkhira </option>
                                                    </optgroup>

                                                    <optgroup label="Rajshahi Division">
                                                        <option value="Bogra">Bogra </option>
                                                        <option value="Chapainananganj">Chapainananganj </option>
                                                        <option value="Joypurhat"> Joypurhat</option>
                                                        <option value="Pabna">Pabna </option>
                                                        <option value="Naogaon"> Naogaon</option>
                                                        <option value="Natore">Natore </option>
                                                        <option value="Rajshahi">Rajshahi </option>
                                                        <option value="Sirajganj"> Sirajganj</option>
                                                    </optgroup>

                                                    <optgroup label="Barishal Division">
                                                        <option value="Barguna">Barguna </option>
                                                        <option value="Barishal"> Barishal</option>
                                                        <option value="Bhola">Bhola </option>
                                                        <option value="Jhalokati"> Jhalokati</option>
                                                        <option value="Patuakhali"> Patuakhali</option>
                                                        <option value="Pirojpur"> Pirojpur</option>
                                                    </optgroup>

                                                    <optgroup label="Sylhet Division">
                                                        <option value="Habiganj"> Habiganj</option>
                                                        <option value="Maulvibazar">Maulvibazar </option>
                                                        <option value="Sunamgonj">Sunamgonj </option>
                                                        <option value="Sylhet">Sylhet </option>
                                                    </optgroup>

                                                    <optgroup label="Rangpur Division">
                                                        <option value="Dinajpur">Dinajpur </option>
                                                        <option value="Gaibandha">Gaibandha  </option>
                                                        <option value="Kurigram">Kurigram </option>
                                                        <option value="Lalmonirhat">Lalmonirhat </option>
                                                        <option value="Nilphamari">Nilphamari </option>
                                                        <option value="Panchagarh">Panchagarh </option>
                                                        <option value="Rangpur">Rangpur </option>
                                                        <option value="Thakurgaon"> Thakurgaon</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="LocalCity">Local City:</label>
                                                <input type="text" name="LocalCity" id="LocalCity" class="form-control" placeholder="Like Chawk Bazar...." style="height: 40px">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="PropertyStatus">Property Statue  :</label>
                                                <select id="PropertyStatus" name="PropertyStatus" class="selectpicker show-tick form-control">
                                                    <option selected disabled> -Status- </option>
                                                    <option value="Rent">Rent </option>
                                                    <option value="Sale">Sale</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 padding-top-15">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="min-bed">Min bed :</label>
                                                <input type="text" name="MinBed" class="span2" value="" data-slider-min="0"
                                                       data-slider-max="20" data-slider-step="1"
                                                       data-slider-value="10" id="min-bed" ><br />
                                                <b class="pull-left color">1</b>
                                                <b class="pull-right color">20</b>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">

                                            <div class="form-group">
                                                <label for="min-baths">Min baths :</label>
                                                <input type="text" name="MinBaths" class="span2" value="" data-slider-min="0"
                                                       data-slider-max="20" data-slider-step="1"
                                                       data-slider-value="10" id="min-baths" ><br />
                                                <b class="pull-left color">1</b>
                                                <b class="pull-right color">20</b>
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="Price-Fair">Price (Tk.):</label>
                                                <input type="number" name="Price" id="Price-Fair" class="form-control" placeholder="Price of the property in Tk..">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 padding-top-15">

                                        <div class="col-sm-12">
                                            <label>Extra Facilities :</label>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="Full Furnished" name="Facilities"> Full Furnished
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="Semi Furnished" name="Facilities"> Semi Furnished
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="not Furnished" name="Facilities"> Not Furnished
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- End step 2 -->

                            <div class="tab-pane" id="step3">
                                <h4 class="info-text">Give us somme images and videos ? </h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="property-images">Chose Images :</label>
                                            <input class="form-control" data-minlength="4" name="PropertyPicture[]" type="file" id="property-images" multiple required>
                                            <p class="help-block">Select multiple images for your property .</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="property-video">Property video :</label>
                                            <input class="form-control" value="" id="property-video" placeholder="http://www.youtube.com, http://vimeo.com" name="property_video" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 3 -->


                            <div class="tab-pane" id="step4">
                                <h4 class="info-text"> Finished and submit </h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="">
                                            <p>
                                                <label><strong>Terms and Conditions</strong></label>
                                                By accessing or using  GARO ESTATE services, such as
                                                posting your property advertisement with your personal
                                                information on our website you agree to the
                                                collection, use and disclosure of your personal information
                                                in the legal proper manner
                                            </p>

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" /> <strong>Accept termes and conditions.</strong>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 4 -->

                        </div>

                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-primary' name='next' value='Next' />
                                <input type='button' class='btn btn-finish btn-primary' id='finish' name='finish' value='Finish' />
                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-default' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                <!-- End submit form -->
            </div>
        </div>
    </div>
</div>
<!-- Footer area-->
<div class="footer-area">

    <div class=" footer">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>About us </h4>
                        <div class="footer-title-line"></div>

                        <img src="../resources/assets/img/footer-logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                        <p>Lorem ipsum dolor cum necessitatibus su quisquam molestias. Vel unde, blanditiis.</p>
                        <ul class="footer-adress">
                            <li><i class="pe-7s-map-marker strong"> </i> PHP-B69, BITM, CTG</li>
                            <li><i class="pe-7s-mail strong"> </i> bengalcoders@gmail.com</li>
                            <li><i class="pe-7s-call strong"> </i> +8801858078583</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links </h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-menu">
                            <li><a href="properties-sale.php">Properties For Sale</a>  </li>
                            <li><a href="properties-rent.php">Properties For Rent</a>  </li>
                            <li><a href="submit-property.php">Submit property </a></li>
                            <li><a href="contact.php">Contact us</a></li>
                            <li><a href="register.php">Log In </a>  </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Last News</h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-blog">
                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="">
                                        <img src="../resources/assets/img/demo/small-proerty-2.jpg">
                                    </a>
                                    <span class="blg-date">12-12-2016</span>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6> <a href="">Add news functions </a></h6>
                                    <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer news-letter">
                        <h4>Stay in touch</h4>
                        <div class="footer-title-line"></div>
                        <p>Lorem ipsum dolor sit amet, nulla  suscipit similique quisquam molestias. Vel unde, blanditiis.</p>

                        <form>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="E-mail ... ">
                                <span class="input-group-btn">
                                            <button class="btn btn-primary subscribe" type="button"><i class="pe-7s-paper-plane pe-2x"></i></button>
                                        </span>
                            </div>
                            <!-- /input-group -->
                        </form>

                        <div class="social pull-right">
                            <ul>
                                <li><a class="wow fadeInUp animated" href="https://twitter.com/kimarotec"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-copy text-center">
        <div class="container">
            <div class="row">
                <div class="pull-left">
                    <span> (C) <a href="http://www.Facebook.com/Hijbuu">Dream Palace</a> , All rights reserved 2016  </span>
                </div>
                <div class="bottom-menu pull-right">
                    <ul>
                        <li><a class="wow fadeInUp animated" href="index.php" data-wow-delay="0.2s">Home</a></li>
                        <li><a class="wow fadeInUp animated" href="properties-sale.php" data-wow-delay="0.3s">Properties for Sale</a></li>
                        <li><a class="wow fadeInUp animated" href="properties-rent.php" data-wow-delay="0.3s">Properties for Rent</a></li>
                        <li><a class="wow fadeInUp animated" href="contact.php" data-wow-delay="0.6s">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="../resources/assets/js/modernizr-2.6.2.min.js"></script>

<script src="../resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="../resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../resources/assets/js/bootstrap-select.min.js"></script>
<script src="../resources/assets/js/bootstrap-hover-dropdown.js"></script>

<script src="../resources/assets/js/easypiechart.min.js"></script>
<script src="../resources/assets/js/jquery.easypiechart.min.js"></script>

<script src="../resources/assets/js/owl.carousel.min.js"></script>
<script src="../resources/assets/js/wow.js"></script>

<script src="../resources/assets/js/icheck.min.js"></script>

<script src="../resources/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="../resources/assets/js/price-range.js"></script>
<script src="../resources/assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="../resources/assets/js/jquery.validate.min.js"></script>
<script src="../resources/assets/js/wizard.js"></script>
<script src="../resources/assets/js/validator.js"></script>

<script src="../resources/assets/js/main.js"></script>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->



<script>

    $(document).ready(function () {

        $('#finish').click(function () {

            $('#SubmitForm').attr('action', 'store.php');
            $('#SubmitForm').submit();


        });

        $('.alert').slideDown("slow").delay(5000).slideUp("slow");
    });
</script>


</body>
</html>