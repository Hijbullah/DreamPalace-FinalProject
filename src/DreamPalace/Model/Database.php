<?php
/**
 * Created by PhpStorm.
 * User: Hijbu
 * Date: 10/16/2017
 * Time: 11:01 PM
 */

namespace App\Model;
use PDO, PDOException;


class Database
{
    protected $dbh;

    public function __construct()
    {
        try{

            $this->dbh = new PDO("mysql:host=localhost; dbname=dream_palace","root","");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //echo "Database connection successful!<br>";

        }catch (PDOException $error){

            echo $error->getMessage();

        }
    }

}