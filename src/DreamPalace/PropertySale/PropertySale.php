<?php
/**
 * Created by PhpStorm.
 * User: Hijbu
 * Date: 10/16/2017
 * Time: 11:12 PM
 */

namespace App\PropertySale;


use App\Model\Database;
use PDO;

class PropertySale extends Database
{
    public $id, $ownersName, $email, $phone, $division, $district, $localCity, $propertyName, $propertyPicture, $description, $minBed, $minBath, $facilities, $propertySize, $propertyPrice, $propertyStatus;

    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];

        if (array_key_exists("OwnersName", $postArray))
            $this->ownersName = $postArray["OwnersName"];

        if(array_key_exists("Email",$postArray))
            $this->email = $postArray["Email"];

        if(array_key_exists("Phone",$postArray))
            $this->phone = $postArray["Phone"];

        if(array_key_exists("Division",$postArray))
                    $this->division = $postArray["Division"];

        if(array_key_exists("District",$postArray))
                    $this->district = $postArray["District"];

        if(array_key_exists("LocalCity",$postArray))
                    $this->localCity = $postArray["LocalCity"];

        if(array_key_exists("PropertyName",$postArray))
                    $this->propertyName = $postArray["PropertyName"];

        if(array_key_exists("PropertyPicture",$postArray))
                    $this->propertyPicture = $postArray["PropertyPicture"];

        if(array_key_exists("Description",$postArray))
                    $this->description = $postArray["Description"];

        if(array_key_exists("MinBed",$postArray))
                    $this->minBed = $postArray["MinBed"];

        if(array_key_exists("MinBaths",$postArray))
                    $this->minBath = $postArray["MinBaths"];

        if(array_key_exists("Facilities",$postArray))
                    $this->facilities = $postArray["Facilities"];

        if(array_key_exists("PropertySize",$postArray))
                    $this->propertySize = $postArray["PropertySize"];

        if(array_key_exists("Price",$postArray))
                    $this->propertyPrice = $postArray["Price"];

        if(array_key_exists("PropertyStatus",$postArray))
                    $this->propertyStatus = $postArray["PropertyStatus"];

    } // end of setData() Method


    public function store(){

        $sqlQuery = "INSERT INTO property_sale ( owners_name, email, phone, division, district, local_city, property_name, property_picture, description, min_bed, min_bath, facilities, property_size, property_price, property_status ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

        $sth = $this->dbh->prepare($sqlQuery);

        $dataArray = [ $this->ownersName, $this->email, $this->phone, $this->division, $this->district, $this->localCity, $this->propertyName, $this->propertyPicture, $this->description, $this->minBed, $this->minBath, $this->facilities, $this->propertySize, $this->propertyPrice, $this->propertyStatus ];

        $status = $sth->execute($dataArray);

        if ($status)
            echo "<script>alert('Data has been inserted Successfully.')</script>";
        else
            echo "<script>alert('Data has not been inserted Successfully.')</script>";
    } // end of store() method


    public function index(){

        $sqlQuerty = "SELECT * FROM property_sale WHERE is_trashed = 'NO' ORDER BY id DESC ";

        $sth = $this->dbh->query($sqlQuerty);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    } // end of index() method

    public function propertyForSale(){

        $sqlQuerty = "SELECT * FROM property_sale WHERE property_status = 'Sale' && is_trashed='NO' ORDER BY id DESC ";

        $sth = $this->dbh->query($sqlQuerty);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    } // end of propertyForSale() method

    public function propertyForRent(){

            $sqlQuerty = "SELECT * FROM property_sale WHERE property_status = 'Rent' && is_trashed='NO' ORDER BY id DESC ";

            $sth = $this->dbh->query($sqlQuerty);

            $sth->setFetchMode(PDO::FETCH_OBJ);
            $allData = $sth->fetchAll();
            return $allData;
        } // end of propertyForRent() method

    public function userProperties($postArray){
            if (isset($postArray['Email'])){
                $sqlQuerty = "SELECT * FROM property_sale WHERE email = '$this->email' && is_trashed='NO' ORDER BY id DESC ";
            }

            $sth = $this->dbh->query($sqlQuerty);

            $sth->setFetchMode(PDO::FETCH_OBJ);
            $allData = $sth->fetchAll();
            return $allData;
        } // end of userProperties() method

    public function view(){

        $sqlQuery = "SELECT * FROM property_sale WHERE id=".$this->id;
        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $oneData = $sth->fetch();
        return $oneData;
    } // end of view() method


    public function propertySalePaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);
        if($start<0) $start = 0;
        $sqlQury = "SELECT * FROM property_sale  WHERE property_status = 'Sale' && is_trashed = 'No'  ORDER BY id DESC LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQury);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();
        return $someData;
    } // end of propertySalePaginator() method

    public function propertyRentPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);
        if($start<0) $start = 0;
        $sqlQury = "SELECT * FROM property_sale  WHERE property_status = 'Rent' && is_trashed = 'No'  ORDER BY id DESC LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQury);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();
        return $someData;
    } // end of propertyRentPaginator() method


    public function userPropertiesPaginator($postArray, $page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);
        if($start<0) $start = 0;

        if (isset($postArray['Email'])){
            $sqlQury = "SELECT * FROM property_sale  WHERE email = '$this->email' && is_trashed = 'No'  ORDER BY id DESC LIMIT $start,$itemsPerPage";
        }



        $sth = $this->dbh->query($sqlQury);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();
        return $someData;
    } // end of userPropertiesPaginator() method

    public function sorting($postArray){


        if (isset($postArray['Division'])){
            $sqlQuery = "SELECT * FROM property_sale WHERE  division=".'"'.$this->division.'"';
        }

        if (isset($postArray['District'])){
            $sqlQuery = "SELECT * FROM property_sale WHERE  district=".'"'.$this->district.'"';
        }
        if (isset($postArray['LocalCity'])){
            $sqlQuery = "SELECT * FROM property_sale WHERE  local_city=".'"'.$this->localCity.'"';
        }

        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchall();
        return $allData;

    } // end of sorting() method



    public function getAllKeywordsIndex()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->property_name);
        }



        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->property_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->description);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords index





    public function searchIndex($requestArray){
        $sql = "";

        if( isset($requestArray['KeyWords']) && !isset($requestArray['Division']) && !isset($requestArray['PropertyStatus'])) $sql = "SELECT * FROM `property_sale` WHERE `is_trashed` ='No' AND (`property_name` LIKE '%".$requestArray['KeyWords']."%' OR `description` LIKE '%".$requestArray['KeyWords']."%')";

        if( isset($requestArray['KeyWords']) && isset($requestArray['Division']) && !isset($requestArray['PropertyStatus'] ))  $sql = "SELECT * FROM `property_sale` WHERE `is_trashed` ='No' AND ((`property_name` LIKE '%".$requestArray['KeyWords']."%' OR `description` LIKE '%".$requestArray['KeyWords']."%') && `division` LIKE '%".$requestArray['Division']."%')";

        if( isset($requestArray['KeyWords']) && isset($requestArray['Division']) && isset($requestArray['PropertyStatus'] ))  $sql = "SELECT * FROM `property_sale` WHERE `is_trashed` ='No' AND ((`property_name` LIKE '%".$requestArray['KeyWords']."%' OR `description` LIKE '%".$requestArray['KeyWords']."%') && `division` LIKE '%".$requestArray['Division']."%' && `property_status` LIKE '%".$requestArray['PropertyStatus']."%' )";



        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()







}