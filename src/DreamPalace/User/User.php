<?php
namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class User extends DB{
    public $table="users";
    public $name="";
    public $email="";
    public $password="";
    public $userId="";
    public $email_token="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }

        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('UserId',$data)){
            $this->userId=$data['UserId'];
        }

        if(array_key_exists('email_token',$data)){
            $this->email_token=$data['email_token'];
        }


        return $this;
    }





    public function store() {


       $dataArray= array(':name'=>$this->name,':email'=>$this->email,':password'=>$this->password, ':email_token'=>$this->email_token);


        $sqlQuery="INSERT INTO `dream_palace`.`users` (`name`, `email`, `password`, `email_verified`) 
VALUES (:name, :email, :password, :email_token)";

        $sth=$this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);
        
        if ($status) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function change_password(){
        $sqlQuery="UPDATE `dream_palace`.`users` SET `password`=:password  WHERE `users`.`email` =:email";
        $sth=$this->dbh->prepare($sqlQuery);
        $status = $sth->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($status){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }

    }

    public function view(){
        $sqlQuery=" SELECT * FROM users WHERE email = '$this->email' ";
       // Utility::dd($query);
        $sth =$this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();

    }// end of view()

    
    public function validTokenUpdate(){
        $sqlQuery="UPDATE `dream_palace`.`users` SET  `email_verified`='".'Yes'."' WHERE `users`.`email` ='$this->email'";
        $sth=$this->dbh->prepare($sqlQuery);
        $status = $sth->execute();

        if($status){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../../../../views/BITM/TeamName/User/Profile/signup.php');
    }

    public function update(){

        $sqlQuery="UPDATE `dream_palace`.`users` SET `name`=:name, `email` =:email, WHERE `users`.`email` = :email";

        $sth=$this->dbh->prepare($sqlQuery);

        $status = $sth->execute(array(':name'=>$this->name, ':email'=>$this->email, ':email_token'=>$this->email_token));

        if($status){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }

}

