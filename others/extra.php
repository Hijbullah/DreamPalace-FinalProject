<div style="display: none;" class="search-toggle">

    <div class="search-row">

        <div class="form-group mar-r-20">
            <label for="price-range">Min baths :</label>
            <input type="text" name="MinBath" class="span2" value="" data-slider-min="0"
                   data-slider-max="20" data-slider-step="1"
                   data-slider-value="10" id="min-baths" ><br />
            <b class="pull-left color">1</b>
            <b class="pull-right color">20</b>
        </div>
        <!-- End of  -->

        <div class="form-group mar-l-20">
            <label for="property-geo">Min bed :</label>
            <input type="text" name="MinBed" class="span2" value="" data-slider-min="0"
                   data-slider-max="20" data-slider-step="1"
                   data-slider-value="10" id="min-bed" ><br />
            <b class="pull-left color">1</b>
            <b class="pull-right color">20</b>
        </div>
        <!-- End of  -->

    </div>
    <br>
    <div class="col-sm-12 text-left">
        <label>Extra Facilities :</label>
    </div>
    <div class="search-row">

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="Facilities[]" value="Furnished"> Furnished
                </label>
            </div>
        </div>
        <!-- End of  -->

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="Facilities[]" value="Semi Furnished"> Semi Furnished
                </label>
            </div>
        </div>
        <!-- End of  -->

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="Facilities[]" value="Not Furnished"> Not Furnished
                </label>
            </div>
        </div>
        <!-- End of  -->
    </div>
</div>










<div class="panel-heading">
    <h3 class="panel-title">Smart search</h3>
</div>
<div class="panel-body search-widget">
    <form action="" class=" form-inline">
        <fieldset>
            <div class="row">
                <div class="col-xs-12">
                    <input type="text" class="form-control" placeholder="Key word">
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="row">
                <div class="col-xs-6">

                    <select id="lunchBegins" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select Your City">

                        <option>New york, CA</option>
                        <option>Paris</option>
                        <option>Casablanca</option>
                        <option>Tokyo</option>
                        <option>Marraekch</option>
                        <option>kyoto , shibua</option>
                    </select>
                </div>
                <div class="col-xs-6">

                    <select id="basic" class="selectpicker show-tick form-control">
                        <option> -Status- </option>
                        <option>Rent </option>
                        <option>Boy</option>
                        <option>used</option>

                    </select>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <label for="price-range">Price range ($):</label>
                    <input type="text" class="span2" value="" data-slider-min="0"
                           data-slider-max="600" data-slider-step="5"
                           data-slider-value="[0,450]" id="price-range" ><br />
                    <b class="pull-left color">2000$</b>
                    <b class="pull-right color">100000$</b>
                </div>
                <div class="col-xs-6">
                    <label for="property-geo">Property geo (m2) :</label>
                    <input type="text" class="span2" value="" data-slider-min="0"
                           data-slider-max="600" data-slider-step="5"
                           data-slider-value="[50,450]" id="property-geo" ><br />
                    <b class="pull-left color">40m</b>
                    <b class="pull-right color">12000m</b>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <label for="price-range">Min baths :</label>
                    <input type="text" class="span2" value="" data-slider-min="0"
                           data-slider-max="600" data-slider-step="5"
                           data-slider-value="[250,450]" id="min-baths" ><br />
                    <b class="pull-left color">1</b>
                    <b class="pull-right color">120</b>
                </div>

                <div class="col-xs-6">
                    <label for="property-geo">Min bed :</label>
                    <input type="text" class="span2" value="" data-slider-min="0"
                           data-slider-max="600" data-slider-step="5"
                           data-slider-value="[250,450]" id="min-bed" ><br />
                    <b class="pull-left color">1</b>
                    <b class="pull-right color">120</b>

                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label> <input type="checkbox" checked> Fire Place</label>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="checkbox">
                        <label> <input type="checkbox"> Dual Sinks</label>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label> <input type="checkbox" checked> Swimming Pool</label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label> <input type="checkbox" checked> 2 Stories </label>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label><input type="checkbox"> Laundry Room </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label> <input type="checkbox"> Emergency Exit</label>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label>  <input type="checkbox" checked> Jog Path </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label>  <input type="checkbox"> 26' Ceilings </label>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset class="padding-5">
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox">
                        <label>  <input type="checkbox"> Hurricane Shutters </label>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset >
            <div class="row">
                <div class="col-xs-12">
                    <input class="button btn largesearch-btn" value="Search" type="submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>
</div>